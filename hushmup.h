#ifndef HUSHMUP_H
#define HUSHMUP_H
#include <cassert>
#include <string>
#include <functional>

using namespace std;
template<typename T, typename H>
class HushMup {
	public:
		bool *occupied;
		T *keys;
		H *vals;
		int (*my_fp)(T);
		int capacity;
		int sz;

		HushMup(int (*fp)(T)){
			this->occupied = new bool[5];
			this->keys = new T[5];
			this->vals = new H[5];
			this->my_fp = fp;
			this->capacity = 5;
			this->sz = 0;
		}
		~HushMup(){
			 delete[] keys; delete[] vals; delete[] occupied;
		}
		H get(T key){
			assert(has(key));
			int i = indexOf(key);
			if(occupied[i]){
				return this->vals[i];
			}else{
				return NULL;
			}
		}
		void put(T key, H val){
			int i = indexOf(key);
			int j = sizeOf();
			if(i == -1){
				if(j+1 == this->capacity){
					growArrays();
					this->keys[j+1] = key;
					this->vals[j+1] = val;
					this->occupied[j+1] = true;
					sz++;
				}
				else{
					this->keys[j+1] = key;
					this->vals[j+1] = val;
					this->occupied[j+1] = true;
					sz++;
				}

			}else{
				if(!occupied[i]){
					this->occupied[i] = true;	
				}
			}
		}
		bool has(T key){
			int i = indexOf(key);
			if(i == -1){
				return false;
			}
			return true;
		}
		int indexOf(T key){
			int done = 0;
			int hash = (my_fp(key)) % capacity;
			while(done != capacity){
				if(hash == capacity){
					hash = 0;
					if(key == keys[hash]){
						if(occupied[hash]){
							return hash;
						}
					}
					hash++;
					done++;
				}else{
					if(key == keys[hash]){
						if(occupied[hash]){
							return hash;
						}
					}		
					hash++;
					done++;
				}
			}
			return -1;
		}
		void del(T key){
			assert(has(key));
			int idx = indexOf(key);
				if(idx== -1){
					
				}else{
					occupied[idx] = false;
				}
			}		
		
		void growArrays(){
			T keystemp[capacity];
			H valstemp[capacity];
			bool occtemp[capacity];
			for(int i = 0; i < capacity; i++){
				keystemp[i] = this->keys[i];
				valstemp[i] = this->vals[i];
				occtemp[i] = this->occupied[i];
			}
			delete[] keys; delete[] vals; delete[] occupied;
			this->keys = new T[capacity*2];
			this->vals = new H[capacity*2];
			this->occupied = new bool[capacity*2];
			for(int i = 0; i <capacity; i++){
				this->keys[i] = keystemp[i];
				this->vals[i] = valstemp[i];
				this->occupied[i] = occtemp[i];
			}
			capacity = 2*capacity;
		}
		int sizeOf(){
			return this->sz;
		}		
};
#endif
