#include <string>
#include "gtest/gtest.h"
#include "hushmup.h"
using namespace std;
int foo(int key){
	return key;
}
int boo(string key){
	int sum = 0;
	for(unsigned int i = 0; i <key.size(); i++){
		sum += (int)key[i];
	}
	return sum; 
}
TEST(HushMup, Hushmup){
	int (*fp)(int);
	fp = foo;
	HushMup<int,int>* hm = new HushMup<int,int>(fp);
	int size = hm->sizeOf();
	ASSERT_EQ(true, size == 0);
	delete hm;
}

TEST(HushMup, put){
	int (*fp)(int);
	fp = foo;
	HushMup<int,int>* hm = new HushMup<int,int>(fp);
	int size = hm->sizeOf();
	hm->put(5,7);
	hm->put(5,7);
	hm->put(5,7);
	size = hm->sizeOf();
	ASSERT_EQ(true, size == 1);
	ASSERT_EQ(true, hm->get(5) == 7);	
	delete hm;
}
TEST(HushMup, has){
	int(*fp)(int);
	fp = foo;
	HushMup<int,int>* hp = new HushMup<int,int>(fp);
	hp->put(1,1);
	hp->put(2,2);
	hp->put(3,3);
	hp->put(4,4);
	hp->put(5,5);
	hp->put(6,6);
	//		EXPECT_EQ(true, hp->has(4));
	//		EXPECT_EQ(true, hp->get(4) == 4);

	int sizw = hp->sizeOf();
	ASSERT_EQ(true, sizw == 6);	
	delete hp;	
}
TEST(HushMup,del){	
	int (*fp)(int);
	fp = foo;
	HushMup<int,int>* hm = new HushMup<int,int>(fp);
	hm->put(5,7);
	hm->put(4,9);
	hm->del(5);
	ASSERT_EQ(false, hm->has(5));
	delete hm;	
}
TEST(HushMup,put2){
  int (*fp)(string);
  fp = boo;
  HushMup<string,string>* hm = new HushMup<string,string>(fp);
  hm->put("hi", "me");
  hm->put("hi", "me");
  ASSERT_EQ(true, hm->sizeOf() == 1);
  delete hm;			
  }
 TEST(HushMup, has2){
  int (*fp)(string);
  fp = boo;
  HushMup<string,string>* hm = new HushMup<string,string>(fp);
  hm->put("yoo","hoowzit");
  EXPECT_EQ(true,hm->has("yoo"));
  EXPECT_EQ(true, hm->get("yoo") == "hoowzit");
  hm->put("hey", "np");
  hm->put("foo","bar");
  hm->put("buzz","bizz");
  hm->put("wee","woo");
  ASSERT_EQ(true, hm->sizeOf() == 5);
  delete hm;
  }

  TEST(HushMup,del2){
	int (*fp)(string);
	fp = boo;
	HushMup<string,string>* hm = new HushMup<string,string>(fp);
	hm->put("me","you");
	hm->put("yes","no");
	EXPECT_EQ(true, hm->has("yes"));
	hm->del("yes");
	ASSERT_EQ(false, hm->has("yes"));
	delete hm;
  }

int main(int argc, char **argv){
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
